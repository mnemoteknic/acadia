EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A 11000 8500
encoding utf-8
Sheet 1 1
Title "ACADIA"
Date "2021-10-21"
Rev "V01"
Comp "mnemoteknic"
Comment1 "https://gitlab.com/mnemoteknic/acadia"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 4600 2500 2    50   ~ 0
U0,1_Tx
Text Label 4600 2600 2    50   ~ 0
U0,1_Rx
Text Label 4600 2700 2    50   ~ 0
GPIO18
Wire Wire Line
	3950 2700 4600 2700
Wire Wire Line
	3950 2600 4600 2600
Wire Wire Line
	3950 2500 4600 2500
Text Label 1450 6200 0    50   ~ 0
DE
Text Label 1450 6100 0    50   ~ 0
~RE
Text Label 1450 6000 0    50   ~ 0
Rx
Text Label 1450 6300 0    50   ~ 0
Tx
Wire Wire Line
	1650 6300 1450 6300
Wire Wire Line
	1650 6200 1450 6200
Wire Wire Line
	1650 6100 1450 6100
Wire Wire Line
	1650 6000 1450 6000
$Comp
L Connector:Conn_01x05_Male J?
U 1 1 61687DE4
P 5500 6300
AR Path="/5515D395/61687DE4" Ref="J?"  Part="1" 
AR Path="/61687DE4" Ref="J2"  Part="1" 
F 0 "J2" H 5650 6650 50  0000 R CNN
F 1 "Conn_01x05_Male" H 5900 6000 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 5500 6300 50  0001 C CNN
F 3 "~" H 5500 6300 50  0001 C CNN
	1    5500 6300
	-1   0    0    1   
$EndComp
Text Label 3300 6300 0    50   ~ 0
Z
Text Label 3300 6400 0    50   ~ 0
Y
Text Label 3300 6200 0    50   ~ 0
B
Text Label 3300 6100 0    50   ~ 0
A
Wire Wire Line
	3500 5900 3400 5900
$Comp
L Device:C C?
U 1 1 61687E36
P 3650 5900
AR Path="/5515D395/61687E36" Ref="C?"  Part="1" 
AR Path="/61687E36" Ref="C1"  Part="1" 
F 0 "C1" V 3398 5900 50  0000 C CNN
F 1 "0.1uF" V 3489 5900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3688 5750 50  0001 C CNN
F 3 "~" H 3650 5900 50  0001 C CNN
	1    3650 5900
	0    1    1    0   
$EndComp
Wire Wire Line
	3250 6300 3300 6300
Wire Wire Line
	3300 6400 3250 6400
$Comp
L power:GND #PWR?
U 1 1 61687E43
P 3900 6550
AR Path="/5515D395/61687E43" Ref="#PWR?"  Part="1" 
AR Path="/61687E43" Ref="#PWR07"  Part="1" 
F 0 "#PWR07" H 3900 6300 50  0001 C CNN
F 1 "GND" H 3905 6377 50  0000 C CNN
F 2 "" H 3900 6550 50  0001 C CNN
F 3 "" H 3900 6550 50  0001 C CNN
	1    3900 6550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 5700 3400 5900
Connection ~ 3400 5900
Wire Wire Line
	3250 5900 3400 5900
$Comp
L power:+3.3V #PWR?
U 1 1 61687E4C
P 3400 5700
AR Path="/5515D395/61687E4C" Ref="#PWR?"  Part="1" 
AR Path="/61687E4C" Ref="#PWR04"  Part="1" 
F 0 "#PWR04" H 3400 5550 50  0001 C CNN
F 1 "+3.3V" H 3415 5873 50  0000 C CNN
F 2 "" H 3400 5700 50  0001 C CNN
F 3 "" H 3400 5700 50  0001 C CNN
	1    3400 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 3000 2050 3000
$Comp
L power:+3.3V #PWR?
U 1 1 61687E57
P 1550 2900
AR Path="/5515D395/61687E57" Ref="#PWR?"  Part="1" 
AR Path="/61687E57" Ref="#PWR01"  Part="1" 
F 0 "#PWR01" H 1550 2750 50  0001 C CNN
F 1 "+3.3V" H 1565 3073 50  0000 C CNN
F 2 "" H 1550 2900 50  0001 C CNN
F 3 "" H 1550 2900 50  0001 C CNN
	1    1550 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 6400 1500 6400
Wire Wire Line
	1500 6400 1500 6500
Wire Wire Line
	1650 6500 1500 6500
Connection ~ 1500 6500
Wire Wire Line
	1500 6500 1500 6650
$Comp
L power:GND #PWR?
U 1 1 61687E62
P 1500 6650
AR Path="/5515D395/61687E62" Ref="#PWR?"  Part="1" 
AR Path="/61687E62" Ref="#PWR03"  Part="1" 
F 0 "#PWR03" H 1500 6400 50  0001 C CNN
F 1 "GND" H 1505 6477 50  0000 C CNN
F 2 "" H 1500 6650 50  0001 C CNN
F 3 "" H 1500 6650 50  0001 C CNN
	1    1500 6650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 2400 4150 2400
Wire Wire Line
	4150 2400 4150 2800
Connection ~ 4150 3100
Wire Wire Line
	3950 3100 4150 3100
Wire Wire Line
	4150 3100 4150 3600
Wire Wire Line
	4150 3600 4150 3800
Connection ~ 4150 3600
Wire Wire Line
	3950 3600 4150 3600
Wire Wire Line
	4150 3800 3950 3800
Connection ~ 4150 3800
Wire Wire Line
	4150 4250 4150 3800
$Comp
L power:GND #PWR?
U 1 1 61687E73
P 4150 4250
AR Path="/5515D395/61687E73" Ref="#PWR?"  Part="1" 
AR Path="/61687E73" Ref="#PWR06"  Part="1" 
F 0 "#PWR06" H 4150 4000 50  0001 C CNN
F 1 "GND" H 4155 4077 50  0000 C CNN
F 2 "" H 4150 4250 50  0001 C CNN
F 3 "" H 4150 4250 50  0001 C CNN
	1    4150 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 3400 1850 2600
Wire Wire Line
	2050 3400 1850 3400
Connection ~ 1850 3400
Wire Wire Line
	1850 4100 1850 3400
Wire Wire Line
	2050 4100 1850 4100
Wire Wire Line
	1850 2600 2050 2600
Connection ~ 1850 4100
Wire Wire Line
	1850 4250 1850 4100
$Comp
L power:GND #PWR?
U 1 1 61687E81
P 1850 4250
AR Path="/5515D395/61687E81" Ref="#PWR?"  Part="1" 
AR Path="/61687E81" Ref="#PWR02"  Part="1" 
F 0 "#PWR02" H 1850 4000 50  0001 C CNN
F 1 "GND" H 1855 4077 50  0000 C CNN
F 2 "" H 1850 4250 50  0001 C CNN
F 3 "" H 1850 4250 50  0001 C CNN
	1    1850 4250
	1    0    0    -1  
$EndComp
$Comp
L 2021-10-08_04-28-38:ISL83076EIBZA-ND U?
U 1 1 61687E87
P 1650 5900
AR Path="/5515D395/61687E87" Ref="U?"  Part="1" 
AR Path="/61687E87" Ref="U1"  Part="1" 
F 0 "U1" H 2450 6287 60  0000 C CNN
F 1 "ISL83076EIBZA" H 2450 6181 60  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 2450 6140 60  0001 C CNN
F 3 "https://www.renesas.com/us/en/document/dst/isl83071e-isl83072e-isl83073e-isl83075e-isl83076e-isl83077e-isl83078e-isl83070e-datasheet" H 1650 5900 60  0001 C CNN
	1    1650 5900
	1    0    0    -1  
$EndComp
$Comp
L RPi_Hat:RPi_GPIO J1
U 1 1 61687E8D
P 2250 2200
AR Path="/61687E8D" Ref="J1"  Part="1" 
AR Path="/5515D395/61687E8D" Ref="J?"  Part="1" 
F 0 "J1" H 3000 2450 60  0000 C CNN
F 1 "RPi_GPIO" H 3000 2350 60  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x20_P2.54mm_Vertical" H 2250 2200 60  0001 C CNN
F 3 "" H 2250 2200 60  0000 C CNN
	1    2250 2200
	1    0    0    -1  
$EndComp
Text Notes 8000 6950 0    50   ~ 0
Raspberry Pi Pinout From Reference Design:\nhttps://github.com/devbisme/RPi_Hat_Template
Wire Wire Line
	2050 2500 1450 2500
Wire Wire Line
	2050 3600 1450 3600
Text Label 1450 3600 0    50   ~ 0
U3_Rx
Text Label 1450 2500 0    50   ~ 0
U3_Tx
Wire Wire Line
	3950 3300 4600 3300
Wire Wire Line
	2050 3200 1450 3200
Text Label 4600 3300 2    50   ~ 0
U4_Tx
Text Label 1450 3200 0    50   ~ 0
U4_Rx
Wire Wire Line
	3950 3700 4600 3700
Text Label 4600 3700 2    50   ~ 0
U5_Tx
Wire Wire Line
	2050 3800 1450 3800
Text Label 1450 3800 0    50   ~ 0
U5_Rx
$Comp
L Device:R R?
U 1 1 6171A1C3
P 9300 4450
AR Path="/5515D395/6171A1C3" Ref="R?"  Part="1" 
AR Path="/6171A1C3" Ref="R4"  Part="1" 
F 0 "R4" V 9507 4450 50  0000 C CNN
F 1 "47k" V 9416 4450 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9230 4450 50  0001 C CNN
F 3 "~" H 9300 4450 50  0001 C CNN
	1    9300 4450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9150 4450 8950 4450
Wire Wire Line
	9450 4450 9550 4450
$Comp
L power:+3.3V #PWR?
U 1 1 6171A1CC
P 9550 4450
AR Path="/5515D395/6171A1CC" Ref="#PWR?"  Part="1" 
AR Path="/6171A1CC" Ref="#PWR09"  Part="1" 
F 0 "#PWR09" H 9550 4300 50  0001 C CNN
F 1 "+3.3V" H 9565 4623 50  0000 C CNN
F 2 "" H 9550 4450 50  0001 C CNN
F 3 "" H 9550 4450 50  0001 C CNN
	1    9550 4450
	0    1    1    0   
$EndComp
Text Label 8550 4650 0    50   ~ 0
DE
Text Label 7400 4450 0    50   ~ 0
Half_~RE~_DE
$Comp
L Device:R R?
U 1 1 616C940B
P 9300 5000
AR Path="/5515D395/616C940B" Ref="R?"  Part="1" 
AR Path="/616C940B" Ref="R5"  Part="1" 
F 0 "R5" V 9507 5000 50  0000 C CNN
F 1 "47k" V 9416 5000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9230 5000 50  0001 C CNN
F 3 "~" H 9300 5000 50  0001 C CNN
	1    9300 5000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9150 5000 8950 5000
Wire Wire Line
	9450 5000 9550 5000
Text Label 8550 5200 0    50   ~ 0
~RE
$Comp
L power:GND #PWR?
U 1 1 616CD24D
P 9550 5000
AR Path="/5515D395/616CD24D" Ref="#PWR?"  Part="1" 
AR Path="/616CD24D" Ref="#PWR010"  Part="1" 
F 0 "#PWR010" H 9550 4750 50  0001 C CNN
F 1 "GND" H 9555 4827 50  0000 C CNN
F 2 "" H 9550 5000 50  0001 C CNN
F 3 "" H 9550 5000 50  0001 C CNN
	1    9550 5000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7900 4450 7400 4450
Text Notes 7900 3000 0    50   ~ 0
Half Duplex Left / Full Duplex Right\n
Wire Wire Line
	9350 3300 8950 3300
Text Label 8550 3550 0    50   ~ 0
Y
Wire Wire Line
	9350 3850 8950 3850
Text Label 8550 4100 0    50   ~ 0
Z
Text Label 7900 3300 0    50   ~ 0
A
Text Label 7900 3850 0    50   ~ 0
B
Text Label 9350 3300 2    50   ~ 0
Full_Y
Text Label 9350 3850 2    50   ~ 0
Full_Z
Wire Wire Line
	3250 6100 5100 6100
Wire Wire Line
	3250 6200 5050 6200
Wire Wire Line
	3900 5900 3800 5900
Wire Wire Line
	3900 5900 3900 6300
Wire Wire Line
	3900 6300 5000 6300
Connection ~ 3900 6300
Wire Wire Line
	3900 6300 3900 6550
Wire Wire Line
	5300 6400 4950 6400
Wire Wire Line
	5300 6500 4900 6500
Text Label 4350 6400 0    50   ~ 0
Full_Z
Text Label 4350 6500 0    50   ~ 0
Full_Y
$Comp
L Jumper:SolderJumper_2_Open JP4
U 1 1 61713DDA
P 6500 2800
F 0 "JP4" H 6500 2913 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 6500 2914 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 6500 2800 50  0001 C CNN
F 3 "~" H 6500 2800 50  0001 C CNN
	1    6500 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 2800 6000 2800
Wire Wire Line
	6650 2800 6750 2800
Wire Wire Line
	6650 3250 6750 3250
Text Label 6000 3250 0    50   ~ 0
U0,1_Rx
$Comp
L Jumper:SolderJumper_2_Open JP6
U 1 1 617192BC
P 6500 3500
F 0 "JP6" H 6500 3613 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 6500 3614 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 6500 3500 50  0001 C CNN
F 3 "~" H 6500 3500 50  0001 C CNN
	1    6500 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 3500 6000 3500
Wire Wire Line
	6650 3500 6750 3500
Text Label 6000 3500 0    50   ~ 0
U3_Rx
$Comp
L Jumper:SolderJumper_2_Open JP7
U 1 1 617192C6
P 6500 3750
F 0 "JP7" H 6500 3863 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 6500 3864 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 6500 3750 50  0001 C CNN
F 3 "~" H 6500 3750 50  0001 C CNN
	1    6500 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 3750 6000 3750
Wire Wire Line
	6650 3750 6750 3750
Text Label 6000 3750 0    50   ~ 0
U4_Rx
$Comp
L Jumper:SolderJumper_2_Open JP8
U 1 1 617192D0
P 6500 4000
F 0 "JP8" H 6500 4113 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 6500 4114 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 6500 4000 50  0001 C CNN
F 3 "~" H 6500 4000 50  0001 C CNN
	1    6500 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 4000 6000 4000
Wire Wire Line
	6650 4000 6750 4000
Text Label 6000 4000 0    50   ~ 0
U5_Rx
Wire Wire Line
	6750 3250 6750 3500
Wire Wire Line
	6750 3500 6750 3750
Connection ~ 6750 3500
Wire Wire Line
	6750 3750 6750 4000
Connection ~ 6750 3750
Wire Wire Line
	6750 3250 6950 3250
Connection ~ 6750 3250
Text Label 6950 3250 2    50   ~ 0
Rx
Connection ~ 7900 4450
Wire Wire Line
	7900 4450 7900 5000
Wire Wire Line
	3950 2800 4150 2800
Connection ~ 4150 2800
Wire Wire Line
	4150 2800 4150 3100
Wire Wire Line
	3950 2900 4600 2900
Wire Wire Line
	3950 3000 4600 3000
Wire Wire Line
	3950 3200 4600 3200
Text Label 4600 3200 2    50   ~ 0
GPIO25
Text Label 4600 3000 2    50   ~ 0
GPIO24
Text Label 4600 2900 2    50   ~ 0
GPIO23
$Comp
L Jumper:SolderJumper_2_Open JP9
U 1 1 617E1EDB
P 6500 4450
F 0 "JP9" H 6500 4563 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 6500 4564 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 6500 4450 50  0001 C CNN
F 3 "~" H 6500 4450 50  0001 C CNN
	1    6500 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 4450 6000 4450
Wire Wire Line
	6650 4450 6750 4450
Text Label 6000 4450 0    50   ~ 0
GPIO18
$Comp
L Jumper:SolderJumper_2_Open JP10
U 1 1 617E1EE4
P 6500 4700
F 0 "JP10" H 6500 4813 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 6500 4814 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 6500 4700 50  0001 C CNN
F 3 "~" H 6500 4700 50  0001 C CNN
	1    6500 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 4700 6000 4700
Wire Wire Line
	6650 4700 6750 4700
Text Label 6000 4700 0    50   ~ 0
GPIO23
$Comp
L Jumper:SolderJumper_2_Open JP11
U 1 1 617E1EED
P 6500 4950
F 0 "JP11" H 6500 5063 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 6500 5064 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 6500 4950 50  0001 C CNN
F 3 "~" H 6500 4950 50  0001 C CNN
	1    6500 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 4950 6000 4950
Wire Wire Line
	6650 4950 6750 4950
Text Label 6000 4950 0    50   ~ 0
GPIO24
$Comp
L Jumper:SolderJumper_2_Open JP12
U 1 1 617E1EF6
P 6500 5200
F 0 "JP12" H 6500 5313 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 6500 5314 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 6500 5200 50  0001 C CNN
F 3 "~" H 6500 5200 50  0001 C CNN
	1    6500 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 5200 6000 5200
Wire Wire Line
	6650 5200 6750 5200
Text Label 6000 5200 0    50   ~ 0
GPIO25
Wire Wire Line
	6750 4450 6750 4700
Wire Wire Line
	6750 4700 6750 4950
Connection ~ 6750 4700
Wire Wire Line
	6750 4950 6750 5200
Connection ~ 6750 4950
Wire Wire Line
	6750 4450 7200 4450
Connection ~ 6750 4450
Text Label 7200 4450 2    50   ~ 0
Half_~RE~_DE
Text Notes 7900 2450 0    50   ~ 0
120Ω Full Duplex Y/Z Termination\nAlways Leave Open for Half Duplex\n
Wire Wire Line
	6750 1900 6750 2050
Wire Wire Line
	6750 1550 6750 1600
$Comp
L power:+3.3V #PWR?
U 1 1 6175C12D
P 6750 1550
AR Path="/5515D395/6175C12D" Ref="#PWR?"  Part="1" 
AR Path="/6175C12D" Ref="#PWR08"  Part="1" 
F 0 "#PWR08" H 6750 1400 50  0001 C CNN
F 1 "+3.3V" H 6765 1723 50  0000 C CNN
F 2 "" H 6750 1550 50  0001 C CNN
F 3 "" H 6750 1550 50  0001 C CNN
	1    6750 1550
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 6175B8B6
P 6750 1750
F 0 "R1" H 6680 1704 50  0000 R CNN
F 1 "47k" H 6680 1795 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6680 1750 50  0001 C CNN
F 3 "~" H 6750 1750 50  0001 C CNN
	1    6750 1750
	1    0    0    1   
$EndComp
Text Label 6950 2050 2    50   ~ 0
Tx
Connection ~ 6750 2050
Wire Wire Line
	6750 2050 6950 2050
Connection ~ 6750 2550
Wire Wire Line
	6750 2800 6750 2550
Wire Wire Line
	6650 2550 6750 2550
Connection ~ 6750 2300
Wire Wire Line
	6750 2300 6750 2550
Wire Wire Line
	6750 2050 6750 2300
Text Label 6000 2800 0    50   ~ 0
U5_Tx
Text Label 6000 2550 0    50   ~ 0
U4_Tx
Wire Wire Line
	6350 2550 6000 2550
$Comp
L Jumper:SolderJumper_2_Open JP3
U 1 1 61710456
P 6500 2550
F 0 "JP3" H 6500 2663 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 6500 2664 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 6500 2550 50  0001 C CNN
F 3 "~" H 6500 2550 50  0001 C CNN
	1    6500 2550
	1    0    0    -1  
$EndComp
Text Label 6000 2300 0    50   ~ 0
U3_Tx
Wire Wire Line
	6650 2300 6750 2300
Wire Wire Line
	6350 2300 6000 2300
$Comp
L Jumper:SolderJumper_2_Open JP2
U 1 1 6170CC72
P 6500 2300
F 0 "JP2" H 6500 2413 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 6500 2414 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 6500 2300 50  0001 C CNN
F 3 "~" H 6500 2300 50  0001 C CNN
	1    6500 2300
	1    0    0    -1  
$EndComp
Text Label 6000 2050 0    50   ~ 0
U0,1_Tx
Wire Wire Line
	6650 2050 6750 2050
Wire Wire Line
	6350 2050 6000 2050
$Comp
L Jumper:SolderJumper_2_Open JP1
U 1 1 616F3A0B
P 6500 2050
F 0 "JP1" H 6500 2163 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 6500 2164 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 6500 2050 50  0001 C CNN
F 3 "~" H 6500 2050 50  0001 C CNN
	1    6500 2050
	1    0    0    -1  
$EndComp
Text Notes 7900 1750 0    50   ~ 0
120Ω Termination\n
Text Label 7900 2050 0    50   ~ 0
A
Text Label 7900 2700 0    50   ~ 0
Full_Y
Wire Wire Line
	8150 2700 7900 2700
$Comp
L Jumper:SolderJumper_2_Open JP?
U 1 1 616918EA
P 8300 2700
AR Path="/5515D395/616918EA" Ref="JP?"  Part="1" 
AR Path="/616918EA" Ref="JP14"  Part="1" 
F 0 "JP14" H 8300 2905 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 8300 2814 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 8300 2700 50  0001 C CNN
F 3 "~" H 8300 2700 50  0001 C CNN
	1    8300 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	8150 2050 7900 2050
Text Label 9200 2700 2    50   ~ 0
Full_Z
Text Label 9200 2050 2    50   ~ 0
B
Wire Wire Line
	9000 2050 9200 2050
Wire Wire Line
	9000 2700 9200 2700
Wire Wire Line
	8700 2700 8450 2700
Wire Wire Line
	8700 2050 8450 2050
$Comp
L Device:R R?
U 1 1 616918DD
P 8850 2700
AR Path="/5515D395/616918DD" Ref="R?"  Part="1" 
AR Path="/616918DD" Ref="R3"  Part="1" 
F 0 "R3" V 8643 2700 50  0000 C CNN
F 1 "120" V 8734 2700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8780 2700 50  0001 C CNN
F 3 "~" H 8850 2700 50  0001 C CNN
	1    8850 2700
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 616918D7
P 8850 2050
AR Path="/5515D395/616918D7" Ref="R?"  Part="1" 
AR Path="/616918D7" Ref="R2"  Part="1" 
F 0 "R2" V 8643 2050 50  0000 C CNN
F 1 "120" V 8734 2050 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8780 2050 50  0001 C CNN
F 3 "~" H 8850 2050 50  0001 C CNN
	1    8850 2050
	0    1    1    0   
$EndComp
$Comp
L Jumper:SolderJumper_2_Open JP?
U 1 1 616918D1
P 8300 2050
AR Path="/5515D395/616918D1" Ref="JP?"  Part="1" 
AR Path="/616918D1" Ref="JP13"  Part="1" 
F 0 "JP13" H 8300 2255 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 8300 2164 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 8300 2050 50  0001 C CNN
F 3 "~" H 8300 2050 50  0001 C CNN
	1    8300 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 3250 6350 3250
$Comp
L Jumper:SolderJumper_2_Open JP5
U 1 1 616D99ED
P 6500 3250
F 0 "JP5" H 6500 3363 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 6500 3364 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 6500 3250 50  0001 C CNN
F 3 "~" H 6500 3250 50  0001 C CNN
	1    6500 3250
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Open JP18
U 1 1 616C07D4
P 8300 5000
F 0 "JP18" H 8300 5113 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 8300 5114 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 8300 5000 50  0001 C CNN
F 3 "~" H 8300 5000 50  0001 C CNN
	1    8300 5000
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Open JP17
U 1 1 616C07DA
P 8300 4450
F 0 "JP17" H 8300 4563 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 8300 4564 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 8300 4450 50  0001 C CNN
F 3 "~" H 8300 4450 50  0001 C CNN
	1    8300 4450
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Open JP16
U 1 1 616C07E0
P 8300 3850
F 0 "JP16" H 8300 3963 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 8300 3964 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 8300 3850 50  0001 C CNN
F 3 "~" H 8300 3850 50  0001 C CNN
	1    8300 3850
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Open JP15
U 1 1 616C07E6
P 8300 3300
F 0 "JP15" H 8300 3413 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 8300 3414 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 8300 3300 50  0001 C CNN
F 3 "~" H 8300 3300 50  0001 C CNN
	1    8300 3300
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Open JP22
U 1 1 616D6290
P 8800 5000
F 0 "JP22" H 8800 5113 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 8800 5114 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 8800 5000 50  0001 C CNN
F 3 "~" H 8800 5000 50  0001 C CNN
	1    8800 5000
	-1   0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Open JP21
U 1 1 616D6296
P 8800 4450
F 0 "JP21" H 8800 4563 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 8800 4564 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 8800 4450 50  0001 C CNN
F 3 "~" H 8800 4450 50  0001 C CNN
	1    8800 4450
	-1   0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Open JP20
U 1 1 616D629C
P 8800 3850
F 0 "JP20" H 8800 3963 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 8800 3964 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 8800 3850 50  0001 C CNN
F 3 "~" H 8800 3850 50  0001 C CNN
	1    8800 3850
	-1   0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Open JP19
U 1 1 616D62A2
P 8800 3300
F 0 "JP19" H 8800 3413 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 8800 3414 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_TrianglePad1.0x1.5mm" H 8800 3300 50  0001 C CNN
F 3 "~" H 8800 3300 50  0001 C CNN
	1    8800 3300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7900 3300 8150 3300
Wire Wire Line
	7900 3850 8150 3850
Wire Wire Line
	7900 4450 8150 4450
Wire Wire Line
	7900 5000 8150 5000
Wire Wire Line
	8450 5000 8550 5000
Wire Wire Line
	8650 4450 8550 4450
Wire Wire Line
	8450 3850 8550 3850
Wire Wire Line
	8650 3300 8550 3300
Wire Wire Line
	8550 3300 8550 3550
Connection ~ 8550 3300
Wire Wire Line
	8550 3300 8450 3300
Wire Wire Line
	8550 3850 8550 4100
Connection ~ 8550 3850
Wire Wire Line
	8550 3850 8650 3850
Wire Wire Line
	8550 4450 8550 4650
Connection ~ 8550 4450
Wire Wire Line
	8550 4450 8450 4450
Wire Wire Line
	8550 5000 8550 5200
Connection ~ 8550 5000
Wire Wire Line
	8550 5000 8650 5000
$Comp
L Connector:Conn_01x05_Male J?
U 1 1 61713F38
P 5550 7050
AR Path="/5515D395/61713F38" Ref="J?"  Part="1" 
AR Path="/61713F38" Ref="J3"  Part="1" 
F 0 "J3" H 5700 7400 50  0000 R CNN
F 1 "Conn_01x05_Male" H 5950 6750 50  0000 R CNN
F 2 "Connector_Phoenix_MSTB:PhoenixContact_MSTBVA_2,5_5-G-5,08_1x05_P5.08mm_Vertical" H 5550 7050 50  0001 C CNN
F 3 "~" H 5550 7050 50  0001 C CNN
	1    5550 7050
	-1   0    0    1   
$EndComp
Wire Wire Line
	5350 6850 5100 6850
Wire Wire Line
	5100 6850 5100 6100
Connection ~ 5100 6100
Wire Wire Line
	5100 6100 5300 6100
Wire Wire Line
	5350 6950 5050 6950
Wire Wire Line
	5050 6950 5050 6200
Connection ~ 5050 6200
Wire Wire Line
	5050 6200 5300 6200
Wire Wire Line
	5350 7050 5000 7050
Wire Wire Line
	5000 7050 5000 6300
Connection ~ 5000 6300
Wire Wire Line
	5000 6300 5300 6300
Wire Wire Line
	5350 7150 4950 7150
Wire Wire Line
	4950 7150 4950 6400
Connection ~ 4950 6400
Wire Wire Line
	4950 6400 4350 6400
Wire Wire Line
	5350 7250 4900 7250
Wire Wire Line
	4900 7250 4900 6500
Connection ~ 4900 6500
Wire Wire Line
	4900 6500 4350 6500
Wire Wire Line
	1550 2900 1550 3000
$Comp
L mnemoteknic_ACADIA:LOGO #G1
U 1 1 617E9990
P 6700 6950
F 0 "#G1" H 6700 6816 60  0001 C CNN
F 1 "LOGO" H 6700 7084 60  0001 C CNN
F 2 "" H 6700 6950 50  0001 C CNN
F 3 "" H 6700 6950 50  0001 C CNN
	1    6700 6950
	1    0    0    -1  
$EndComp
$EndSCHEMATC
